package Project.netmine.dev.repo;

import Project.netmine.dev.entities.MoneyClaim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MoneyClaimRepo extends JpaRepository<MoneyClaim, Long> {
    Optional <List<MoneyClaim>> findAllByClientId(Long id);
}
