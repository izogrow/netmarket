package Project.netmine.dev.repo;


import Project.netmine.dev.entities.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepo extends JpaRepository<Product, Long> {
    @Query("select c from Product c")
    Page<Product> findAllPage(Pageable pageable);

    @Query("select p from Product p where p.categoryId = ?1")
    Page<Product> findAllByCategoryId(Long categoryId, Pageable pageable);

    @Query("select p from Product p where lower(p.name) like concat(lower( ?1),'%')")
    Page<Product> searchAllByPartName(String partName, Pageable pageable);

    List<Product> findByClientId(Long clientId);
}
