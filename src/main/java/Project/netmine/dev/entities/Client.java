package Project.netmine.dev.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.math.BigDecimal;

@Entity
@Table
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_generator")
    @SequenceGenerator(name = "client_generator", sequenceName = "client_id_seq", allocationSize = 1)
    private Long id;
    private String lastName;
    private String firstName;

    private String username;
    @JsonIgnore
    private String password;
    private String address;
    private BigDecimal bonus;
    private String status;          // may be user or admin
    private boolean enabled;        // enabled пригимает значение false, еслипользователь забанен. по умолчанию true
    private String imageURL;

    public Client() {
        bonus = new BigDecimal(0);
    }

    @JsonCreator
    public Client( @JsonProperty("firstName") String firstName,
                  @JsonProperty("lastName") String lastName, @JsonProperty("address") String address,
                  @JsonProperty("bonus") BigDecimal bonus, @JsonProperty("status") String status,
                   @JsonProperty("imageURL") String imageURL) {

        this.lastName = lastName;
        this.firstName = firstName;
        this.address = address;
       // this.bonus = bonus;
        this.bonus = new BigDecimal(0);
        this.status = status;
        this.imageURL = imageURL;
    }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() { return firstName; }

    public void setFirstName(String firstName) { this.firstName = firstName; }

    public BigDecimal getBonus() {
        return bonus;
    }

    public void setBonus(BigDecimal bonus) {
        this.bonus = bonus;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isEnabled() { return enabled; }

    public void setEnabled(boolean enabled) { this.enabled = enabled; }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
