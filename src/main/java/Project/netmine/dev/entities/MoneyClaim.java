package Project.netmine.dev.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

/*
    Запрос на деньги.
*/

@Entity
@Table
public class MoneyClaim {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private Long clientId;
    private BigDecimal amount;      // сумма
    private Date creationDate;
    private Date reviewDate;
    private Boolean status;     // Поле status хранит результат рассмотрения заявки.
                                // Принимает значание true, если исполнено, false, если отказано,
                                // и null, если еще не рассмотренно.

    public MoneyClaim() {}

    @JsonCreator
    public MoneyClaim(@JsonProperty("id") Long id, @JsonProperty("clientId") Long clientId,
                      @JsonProperty("amount") BigDecimal amount, @JsonProperty("creationDate") Date creationDate,
                      @JsonProperty("reviewDate") Date reviewDate, @JsonProperty("status") Boolean status) {
        this.id = id;
        this.clientId = clientId;
        this.amount = amount;
        this.creationDate = creationDate;
        this.reviewDate = reviewDate;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(Date reviewDate) {
        this.reviewDate = reviewDate;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
