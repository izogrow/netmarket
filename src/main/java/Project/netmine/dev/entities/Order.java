package Project.netmine.dev.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

@Entity
@Table(name = "order_table")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_generator")
    @SequenceGenerator(name = "order_generator", sequenceName = "order_table_id_seq", allocationSize = 1)
    private Long id;
    private Long clientId;
    private Date gettingDate;
    private Date shippingDate;
    private boolean status;
    private BigDecimal amount;

    public Order(){
    }

    @JsonCreator
    public Order(@JsonProperty("id") Long id, @JsonProperty("client_id") Long clientId,
                 @JsonProperty("getting_date") Date gettingDate, @JsonProperty("shipping_date") Date shippingDate,
                 @JsonProperty("status") boolean status, @JsonProperty("amount") BigDecimal amount) {
        //SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        this.id = id;
        this.clientId = clientId;
        this.gettingDate = gettingDate;
        this.shippingDate = shippingDate;
        this.status = status;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Date getGettingDate() {
        return gettingDate;
    }

    public void setGettingDate(Date gettingDate) {
        this.gettingDate = gettingDate;
    }

    public Date getShippingDate() {
        return shippingDate;
    }

    public void setShippingDate(Date shippingDate) {
        this.shippingDate = shippingDate;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isStatus() {
        return status;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}