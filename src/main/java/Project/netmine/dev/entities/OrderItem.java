package Project.netmine.dev.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "order_item")
public class OrderItem {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_item_generator")
    @SequenceGenerator(name = "order_item_generator", sequenceName = "order_item_id_seq", allocationSize = 1)
    private Long id;
    private Long orderId;
    private Long productId;
    private BigDecimal price;
    private String name;
    private Integer quantity;

    public OrderItem() {

    }

    public OrderItem(@JsonProperty("id") Long id ,@JsonProperty("order_id") Long orderId,
                     @JsonProperty("product_id") Long productId, @JsonProperty("name") String name,
                     @JsonProperty("price") BigDecimal price, @JsonProperty("quantity") Integer quantity) {
        this.id = id;
        this.orderId = orderId;
        this.productId = productId;
        this.price = price;
        this.name = name;
        this.quantity = quantity;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getItemId() {
        return productId;
    }

    public void setItemId(Long productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
