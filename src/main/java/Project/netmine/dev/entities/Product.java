package Project.netmine.dev.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_generator")
    @SequenceGenerator(name = "product_generator", sequenceName = "product_id_seq", allocationSize = 1)
    private Long id;
    private BigDecimal price;
    private String name;
    private Long categoryId;
    private Long clientId;     // client_id
    private String text;       // описание товара
    private Integer quantity; //количество товара
    private Boolean existence; //наличие
    private String imageURL;
    private String code;
    private Boolean valide;

    public Product() {}
//
    @JsonCreator
    public Product( @JsonProperty("name") String name, @JsonProperty("price") BigDecimal price,
                   @JsonProperty("category_id") Long categoryId, @JsonProperty("client_id") Long clientId, @JsonProperty("text") String text,
                    @JsonProperty("quantity") Integer quantity, @JsonProperty("existence") Boolean existence,
                   @JsonProperty("imageURL") String imageURL, @JsonProperty("valide") Boolean valide, @JsonProperty("code") String code) {
        this.name = name;
        this.price = price;
        this.categoryId = categoryId;
        this.clientId = clientId;
        this.text = text;
        this.quantity = quantity;
        this.existence = existence;
        this.imageURL = imageURL;
        this.code = code;
        this.valide = valide;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public void setCategoryId(Long categoryId) { this.categoryId = categoryId; }

    public Long getCategoryId() {
        return categoryId;
    }


    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Boolean getExistence() {
        return existence;
    }

    public void setExistence(Boolean existence) {
        this.existence = existence;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getValide() {
        return valide;
    }

    public void setValide(Boolean valide) {
        this.valide = valide;
    }
}

