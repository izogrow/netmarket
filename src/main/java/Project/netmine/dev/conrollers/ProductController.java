package Project.netmine.dev.conrollers;


import Project.netmine.dev.entities.Product;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import Project.netmine.dev.repo.ProductRepo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("product")
public class ProductController {
    private static final Logger log = LoggerFactory.getLogger(ProductController.class);

    private final ProductRepo productRepo;

    @Autowired
    public ProductController(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }

    @GetMapping("/all")
    @Secured("ROLE_ADMIN")      // этот метот возвращаяет ВСЕ товары разом, такая возможность - только у админа
    public ResponseEntity<List<Product>> getProducts() {
        log.debug("Got request to get all products");
        List<Product> products = productRepo.findAll();
        log.debug("Loaded products: {}", products);
        if (products == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping         // example: /product?page=1&size=10&sort=id,desc&sort=name,asc
    public ResponseEntity<Page<Product>> getProductsPage(Pageable pageable) {
        Page<Product> products = productRepo.findAllPage(pageable);
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/findByCategoryId/{category_id}")
    public ResponseEntity<Page<Product>> getProductsPageByCategory(@PathVariable("category_id") Long categoryId, Pageable pageable) {
        Page<Product> products = productRepo.findAllByCategoryId(categoryId, pageable);
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/search")
    public ResponseEntity<Page<Product>> searchProductsPageByPartName(@RequestParam("name") String name, Pageable pageable) {
        Page<Product> products = productRepo.searchAllByPartName(name, pageable);
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> getProduct(@PathVariable("id") Product product){
        if (product == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @GetMapping("/client/{client_id}")
    public ResponseEntity<List<Product>> getProductsByUser(@PathVariable("client_id") Long clientId){
        List<Product> products = productRepo.findByClientId(clientId);
        if (products == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(products, HttpStatus.OK);
    }



    @PostMapping
    public ResponseEntity<Product> createProduct(@RequestBody Product product) {
        if (product == null  || product.getQuantity() == null || product.getName() == null || product.getPrice() == null ) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        product.setExistence(true);
        product.setValide(false);
        productRepo.save(product);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Product> updateProduct(@PathVariable("id") Product product, @RequestBody Product productNew){
        if( product == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        product.setName(productNew.getName());
        product.setCategoryId(productNew.getCategoryId());
        product.setClientId(productNew.getClientId());
        product.setPrice(productNew.getPrice());
        product.setText(productNew.getText());
        product.setImageURL(productNew.getImageURL());
        product.setValide(productNew.getValide());

        productRepo.save(product);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable("id") Product product) {
        productRepo.delete(product);
    }

    @GetMapping("/add_all_items_from_json")
    public ResponseEntity<?> insertJson() throws IOException {       // функция для добавления товаров из json
        ObjectMapper mapper = new ObjectMapper();

        ClassLoader classLoader = getClass().getClassLoader();

        File file = new File(classLoader.getResource("products.json").getFile());
        BufferedReader br = new BufferedReader(new FileReader(file));
        String st;
        Product product;
        int productsCount = 0;

        while ((st = br.readLine()) != null) {

            if (st.equals("[") || st.equals("]")) continue;
            product = mapper.readValue(st, Product.class);
            ++productsCount;

            Product savedProduct =  productRepo.save(product);
        }
        String result = "Added " + productsCount + " items";
        br.close();

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}