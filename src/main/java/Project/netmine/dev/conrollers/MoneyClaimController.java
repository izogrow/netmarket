package Project.netmine.dev.conrollers;

import Project.netmine.dev.Application;
import Project.netmine.dev.entities.Client;
import Project.netmine.dev.entities.MoneyClaim;
import Project.netmine.dev.repo.ClientRepo;
import Project.netmine.dev.repo.MoneyClaimRepo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("moneyClaim")
public class MoneyClaimController {
    private final MoneyClaimRepo moneyClaimRepo;
    private final ClientRepo clientRepo;

    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    final String AMOUNT_MISMATCH_MESSAGE = "Amount in db is differs from amount in request. Refresh the page!";
    final String CLIENT_NOT_FOUND_MESSAGE = "Client not found.";
    final String MONEY_CLAIM_NOT_FOUND_MESSAGE = "Money claim not found.";

    @Autowired
    public MoneyClaimController(MoneyClaimRepo moneyClaimRepo, ClientRepo clientRepo) {
        this.moneyClaimRepo = moneyClaimRepo;
        this.clientRepo = clientRepo;
    }
    @Secured("ROLE_ADMIN")
    @GetMapping
    public ResponseEntity<List<MoneyClaim>> getMoneyClaims() {
        List<MoneyClaim> moneyClaims = moneyClaimRepo.findAll();

        if (moneyClaims == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(moneyClaims, HttpStatus.OK);
    }

//    @GetMapping("/myClaims")
//    public ResponseEntity<List<MoneyClaim>> getMoneyClaimsByClientId(@RequestParam Long clientId
//            /* @RequestHeader("Authorization") String key*/) {
//        Optional<List<MoneyClaim>> data = moneyClaimRepo.findAllByClientId(clientId);
//
//        if (!data.isPresent()) {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//        List<MoneyClaim> moneyClaims = data.get();
//        return new ResponseEntity<>(moneyClaims, HttpStatus.OK);
//    }

    @PostMapping("/{username}")
    public ResponseEntity<MoneyClaim> createMoneyClaim(@PathVariable("username") String username, @RequestBody MoneyClaim moneyClaim) {
        Optional<Client> data = clientRepo.findByUsername(username);
        if (!data.isPresent()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Date currentDate = new Date();
        Client client = data.get();
        moneyClaim.setClientId(client.getId());
        moneyClaim.setCreationDate(new java.sql.Date(currentDate.getTime()));
        MoneyClaim savedMoneyClaim = moneyClaimRepo.save(moneyClaim);
        return new ResponseEntity<>(savedMoneyClaim, HttpStatus.OK);  // сгенерированный базой айди запроса лежит в сущности
    }


    /*
        updateMoneyClaimAmount() - метод, позоляющий клиентам изменять сумму в запросе денег до его рассмотрения админом
     */
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @PutMapping
    public ResponseEntity<MoneyClaim> updateMoneyClaimAmount(@RequestParam("moneyClaimId") Long moneyClaimId,
                                                             @RequestBody MoneyClaim newMoneyClaim) {

        Optional<MoneyClaim> data = moneyClaimRepo.findById(moneyClaimId);
        if (!data.isPresent()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        MoneyClaim moneyClaim = data.get();

        if (moneyClaim.getStatus() != null) {
            // logger.debug("Client {}");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        moneyClaim.setAmount(newMoneyClaim.getAmount());
        moneyClaim.setClientId(newMoneyClaim.getClientId());
        moneyClaim.setCreationDate(newMoneyClaim.getCreationDate());
        moneyClaim.setReviewDate(newMoneyClaim.getReviewDate());


        moneyClaimRepo.save(moneyClaim);
        return new ResponseEntity<>(moneyClaim, HttpStatus.OK);
    }

    @Secured("ROLE_ADMIN")
    @PutMapping("/review")
    public ResponseEntity<?> reviewMoneyClaim(@RequestParam("moneyClaimId") Long moneyClaimId,
                                              @RequestParam("decision") Boolean decision,
                                              @RequestParam("visibleAmount") BigDecimal visibleAmount) {
        Optional<MoneyClaim> moneyClaimData = moneyClaimRepo.findById(moneyClaimId);

        if (!moneyClaimData.isPresent()) {
            return new ResponseEntity<>(MONEY_CLAIM_NOT_FOUND_MESSAGE, HttpStatus.BAD_REQUEST);
        }

        MoneyClaim moneyClaim = moneyClaimData.get();
        if (moneyClaim.getAmount().compareTo(visibleAmount)!=0) {  //.compareTo возвращает 0 если a==b
            return new ResponseEntity<>(AMOUNT_MISMATCH_MESSAGE, HttpStatus.BAD_REQUEST);
        }
        moneyClaim.setStatus(decision);

        Date currentDate = new Date();
        moneyClaim.setReviewDate(new java.sql.Date(currentDate.getTime()));

        if (decision) {                       // в этом блоке мы добавляем клиенту деньги, если решение положительное
            Optional<Client> clientData = clientRepo.findById(moneyClaim.getClientId());
            if (!clientData.isPresent()) {
                return new ResponseEntity<>(CLIENT_NOT_FOUND_MESSAGE, HttpStatus.BAD_REQUEST);
            }

            Client client = clientData.get();

            client.setBonus(client.getBonus().add(moneyClaim.getAmount()));
            clientRepo.save(client);
        }

        moneyClaimRepo.save(moneyClaim);
        return new ResponseEntity<>(moneyClaim, HttpStatus.OK);
    }

    @GetMapping("/myClaims")
    public ResponseEntity<List<MoneyClaim>> getMyMoneyClaims() {
        String username;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal instanceof UserDetails){
            username = ((UserDetails)principal).getUsername();
        }
        else{
            username=principal.toString();
        }
        Optional<Client> clientData = clientRepo.findByUsername(username);
        if (!clientData.isPresent()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Client client = clientData.get();
        Optional<List<MoneyClaim>> moneyClaimData = moneyClaimRepo.findAllByClientId(client.getId());
        if (!moneyClaimData.isPresent()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<MoneyClaim> moneyClaims = moneyClaimData.get();
        return new ResponseEntity<>(moneyClaims, HttpStatus.OK);
    }
}
