package Project.netmine.dev.conrollers;

import Project.netmine.dev.entities.Order;
import Project.netmine.dev.repo.OrderRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("order")
public class OrderController {

    private final OrderRepo orderRepo;

    @Autowired
    public OrderController(OrderRepo orderRepo) {
        this.orderRepo = orderRepo;
    }

    @Secured("ROLE_ADMIN")
    @GetMapping
    public ResponseEntity<List<Order>> getOrders() {
        List<Order> orders = orderRepo.findAll();
        if (orders == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @GetMapping("findAll/{client_id}")
    public ResponseEntity<List<Order>> getOrdersByClient(@PathVariable("client_id") Long clientId) {
        List<Order> orders = orderRepo.findByClientId(clientId);
        if (orders == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @GetMapping("findById/{id}")
    public ResponseEntity<Order> getOrder(@PathVariable("id") Order order){
        if(order == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        orderRepo.save(order);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }



    @GetMapping("/{client_id}")
    public ResponseEntity<Order> getOrder(@PathVariable("client_id") Long clientId){
        List<Order> orders = orderRepo.findByClientId(clientId);
        Order order = null;
        for(int i = 0; i < orders.size(); i++){
            if(!orders.get(i).getStatus())
                order = orders.get(i);
        }
        if (order == null) {
            order = new Order();
            order.setStatus(false);
            order.setClientId(clientId);
            order.setAmount(new BigDecimal(0));
            orderRepo.save(order);
        }
        return new ResponseEntity<>(order, HttpStatus.OK);
    }
//
    @PostMapping
    public ResponseEntity<Order> createOrder(@RequestBody Order order) {
        if (order == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        //  Проверяем, есть ли строки с данным client_id и status false.
        // Если есть, добавляем в этот заказ. Если нет, создаём новый.
        // + проверка на совпадение товара в заказе

        orderRepo.save(order);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Order> updateOrder(@PathVariable("id") Order order, @RequestBody Order newOrder){
        if( order == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        order.setAmount(newOrder.getAmount());
        order.setGettingDate(newOrder.getGettingDate());
        order.setStatus(newOrder.getStatus());
        order.setShippingDate(newOrder.getShippingDate());
        orderRepo.save(order);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void deleteOrder(@PathVariable("id") Order order) {
        orderRepo.delete(order);
    }
}
