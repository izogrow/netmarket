package Project.netmine.dev.conrollers;

import Project.netmine.dev.entities.Category;
import Project.netmine.dev.entities.Client;
import Project.netmine.dev.repo.CategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("category")
public class CategoryController {
    private final CategoryRepo categoryRepo;

    @Autowired
    public CategoryController(CategoryRepo categoryRepo) {
        this.categoryRepo = categoryRepo;
    }

    @GetMapping
    public ResponseEntity<List<Category>> getCategories() {
        List<Category> categories = categoryRepo.findAll();
        if (categories == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

    @GetMapping("/{name}")
    public ResponseEntity<Category> getCategory(@PathVariable("name") String name){
        Category category = categoryRepo.findByName(name);
        if (category == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(category, HttpStatus.OK);
    }

    @GetMapping("findById/{id}")
    public ResponseEntity<Category> getCategoryById(@PathVariable("id") Category category){
        if (category == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(category, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Category> createCategory(@RequestBody Category category) {
        if (category == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        categoryRepo.save(category);
        return new ResponseEntity<>(category, HttpStatus.OK);
    }
//
    @PutMapping("/{id}")
    public ResponseEntity<Category> updateCategory(@PathVariable("id") Category category, @RequestBody Category categoryNew){
        if( category == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        category.setName(categoryNew.getName());
        category.setId(categoryNew.getId());
        categoryRepo.save(category);
        return new ResponseEntity<>(category, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public void deleteCategory(@PathVariable("id") Category category) {
        categoryRepo.delete(category);
    }
}
