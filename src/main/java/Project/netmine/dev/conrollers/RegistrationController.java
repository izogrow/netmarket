package Project.netmine.dev.conrollers;

import Project.netmine.dev.entities.Client;
import Project.netmine.dev.repo.ClientRepo;
import Project.netmine.dev.security.ClientServiceImpl;
import Project.netmine.dev.security.UserCred;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RegistrationController {
    @Autowired
    private ClientServiceImpl clientService;

    final String USERNAME_EXIST_MESSAGE = "Username already exist.";

    @PostMapping("/registration")
    public ResponseEntity<?> addUser(@RequestBody UserCred client) throws Exception{
        if (clientService.isExist(client.getUsername())) {
            return new ResponseEntity<>(USERNAME_EXIST_MESSAGE, HttpStatus.BAD_REQUEST);
        }
        Client savedClient = clientService.save(client);
        return new ResponseEntity(savedClient, HttpStatus.OK);
}
}
