package Project.netmine.dev.conrollers;


import Project.netmine.dev.entities.Client;
import Project.netmine.dev.repo.ClientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("client")
public class ClientController {

    private final ClientRepo clientRepo;

    @Autowired
    public ClientController(ClientRepo clientRepo) {
        this.clientRepo = clientRepo;
    }

    @Secured("ROLE_ADMIN")
    @GetMapping
    public ResponseEntity<List<Client>> getClients() {
        List<Client> clients = clientRepo.findAll();
        if (clients == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(clients, HttpStatus.OK);
    }



    @GetMapping("/{username}")
    public ResponseEntity<Client> getClient(@PathVariable("username") String username){
        Optional<Client> data = clientRepo.findByUsername(username);
        if (!data.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Client client = data.get();
        return new ResponseEntity<>(client, HttpStatus.OK);
    }

    @GetMapping("findById/{id}")
    public ResponseEntity<Client> getClientById(@PathVariable("id") Client client){
        if (client == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(client, HttpStatus.OK);
    }


    

    @PostMapping("/register")
    public ResponseEntity<Client> createClient() {  // это метод не нужен, у нас есть контроллер для регистрации
        Client client = new Client();
        clientRepo.save(client);
        return new ResponseEntity<>(client, HttpStatus.OK);
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @PutMapping("/{username}")
    public ResponseEntity<Client> updateClient(@PathVariable("username") String username, @RequestBody Client clientNew){
        Optional<Client> data = clientRepo.findByUsername(username);
        if(!data.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Client client = data.get();
        client.setFirstName(clientNew.getFirstName());
        client.setLastName(clientNew.getLastName());
        client.setAddress(clientNew.getAddress());
        //client.setBonus(clientNew.getBonus());
        client.setEnabled(clientNew.isEnabled());
        client.setImageURL(clientNew.getImageURL());

        clientRepo.save(client);
        return new ResponseEntity<>(client, HttpStatus.OK);
    }

    @PutMapping("/putById/{id}")
    public ResponseEntity<Client> updateClientById(@PathVariable("id") Client client, @RequestBody Client clientNew){
        if(client == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        client.setFirstName(clientNew.getFirstName());
        client.setLastName(clientNew.getLastName());
        client.setAddress(clientNew.getAddress());
        //client.setBonus(clientNew.getBonus());
        client.setImageURL(clientNew.getImageURL());
        clientRepo.save(client);
        return new ResponseEntity<>(client, HttpStatus.OK);
    }

    @Secured("ROLE_ADMIN")
    @PutMapping("/money")
    public ResponseEntity<Client> addMoney(@RequestBody Long clientId, @RequestBody BigDecimal money) {
        Optional<Client> data = clientRepo.findById(clientId);
        if(!data.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Client client = data.get();
        client.setBonus(client.getBonus().add(money));

        clientRepo.save(client);
        return new ResponseEntity<>(client, HttpStatus.OK);
    }

    @Secured("ROLE_ADMIN")
    @PutMapping("/role")
    public ResponseEntity<Client> grantToAdmin(@RequestBody Long clientId) {
        Optional<Client> data = clientRepo.findById(clientId);
        if(!data.isPresent()){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Client client = data.get();
        client.setStatus("ADMIN");
        clientRepo.save(client);

        return new ResponseEntity<>(client, HttpStatus.OK);
    }


    @DeleteMapping("/{id}")
    public void deleteClient(@PathVariable("id") Client client) {
        clientRepo.delete(client);
    }

}
