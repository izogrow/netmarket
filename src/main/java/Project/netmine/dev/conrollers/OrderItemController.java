package Project.netmine.dev.conrollers;

import Project.netmine.dev.entities.Order;
import Project.netmine.dev.entities.OrderItem;
import Project.netmine.dev.repo.OrderItemRepo;
import Project.netmine.dev.repo.OrderRepo;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("orderItem")
public class OrderItemController {
    private final OrderItemRepo orderItemRepo;
//    private final OrderRepo orderRepo;

    @Autowired
    public OrderItemController(OrderItemRepo orderItemRepo) {
        this.orderItemRepo = orderItemRepo;
//        this.orderRepo = orderRepo;
    }

    @GetMapping
    public ResponseEntity<List<OrderItem>> getOrderItems() {
        List<OrderItem> orderItems = orderItemRepo.findAll();
        if (orderItems == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(orderItems, HttpStatus.OK);
    }

    @GetMapping("/{order_id}")
    public  ResponseEntity<List<OrderItem>> getOrderItem(@PathVariable("order_id") Long orderId){
        List<OrderItem> orderItems = orderItemRepo.findByOrderId(orderId);
        if (orderItems == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(orderItems, HttpStatus.OK);
    }

    @GetMapping("/findByProduct/{product_id}")
    public  ResponseEntity<List<OrderItem>> getOrderItemsByProduct(@PathVariable("product_id") Long productId){
        List<OrderItem> orderItems = orderItemRepo.findByProductId(productId);
        if (orderItems == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(orderItems, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<OrderItem> createOrderItem(@RequestBody OrderItem orderItem) {
        if (orderItem == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        orderItemRepo.save(orderItem);

        return new ResponseEntity<>(orderItem, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<OrderItem> updateOrderItem(@PathVariable("id") OrderItem orderItem, @RequestBody OrderItem orderItemNew){
        if( orderItem == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        orderItem.setId(orderItemNew.getId());
        orderItem.setPrice(orderItemNew.getPrice());
        orderItem.setItemId(orderItemNew.getItemId());
        orderItem.setOrderId(orderItemNew.getOrderId());
        orderItem.setName(orderItemNew.getName());
        orderItem.setQuantity(orderItemNew.getQuantity());
        orderItemRepo.save(orderItem);
        return new ResponseEntity<>(orderItem, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<OrderItem> deleteOrderItem(@PathVariable("id") OrderItem orderItem) {
        orderItemRepo.delete(orderItem);
        return new ResponseEntity<>(orderItem, HttpStatus.OK);
    }
}
