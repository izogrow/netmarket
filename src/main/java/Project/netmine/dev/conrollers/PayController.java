package Project.netmine.dev.conrollers;

import Project.netmine.dev.entities.Client;
import Project.netmine.dev.entities.Order;
import Project.netmine.dev.entities.OrderItem;
import Project.netmine.dev.entities.Product;
import Project.netmine.dev.repo.ClientRepo;
import Project.netmine.dev.repo.OrderItemRepo;
import Project.netmine.dev.repo.OrderRepo;
import Project.netmine.dev.repo.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("pay")
public class PayController {
    private final OrderRepo orderRepo;
    private final ClientRepo clientRepo;
    private final OrderItemRepo orderItemRepo;
    private final ProductRepo productRepo;

    private final String LACK_MONEY_MESSAGE = "У вас недостаточно денег";
    private final String LACK_PRODUCT_MESSAGE = "Такого количества товаров нет в наличии";


    @Autowired
    public PayController(OrderRepo orderRepo, ClientRepo clientRepo, OrderItemRepo orderItemRepo, ProductRepo productRepo) {
        this.orderRepo = orderRepo;
        this.clientRepo = clientRepo;
        this.orderItemRepo = orderItemRepo;
        this.productRepo = productRepo;
    }


    @PutMapping("/{id}")
    public ResponseEntity updateOrder(@PathVariable Long id) {
        Optional<Order> data = orderRepo.findById(id);
        if (!data.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Order order = data.get();
        List<OrderItem> orderItems = orderItemRepo.findByOrderId(order.getId());
        if(orderItems == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        String username;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
            username = principal.toString();
        }

        Client client = clientRepo.findByUsername(username).get();

        if(client.getBonus().compareTo(order.getAmount()) == -1){
            return new ResponseEntity<>(LACK_MONEY_MESSAGE, HttpStatus.BAD_REQUEST);
        }

        //списываем бонусы у покупателя
        client.setBonus(client.getBonus().subtract(order.getAmount()));

        //меняем статус заказа на выполненный
        order.setStatus(true);

        //зачисляем деньги продавцам
        for(OrderItem orderItem: orderItems){
            Product product = productRepo.findById(orderItem.getProductId()).get();
            if(product.getQuantity()< orderItem.getQuantity()){
                return new ResponseEntity<>(LACK_PRODUCT_MESSAGE, HttpStatus.BAD_REQUEST);
            }

            product.setQuantity(product.getQuantity()- orderItem.getQuantity());
            if(product.getQuantity() == 0){
                product.setExistence(false);
            }

            Client seller = clientRepo.findById(product.getClientId()).get();
            seller.setBonus(seller.getBonus().add(orderItem.getPrice()));
            clientRepo.save(seller);
        }

        clientRepo.save(client);
        orderRepo.save(order);

        return new ResponseEntity<>(order, HttpStatus.OK);
    }
}


