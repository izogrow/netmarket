package Project.netmine.dev.security;

import Project.netmine.dev.entities.Client;
import Project.netmine.dev.repo.ClientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Optional;

@Service
public class ClientServiceImpl implements UserDetailsService {

    private final ClientRepo clientRepo;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Autowired
    public ClientServiceImpl(ClientRepo clientRepo) {
        this.clientRepo = clientRepo;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Optional<Client> data = clientRepo.findByUsername(userName);
        data.orElseThrow(() -> new UsernameNotFoundException("User: " + userName + " not found"));
        Client client = data.get();
        return new org.springframework.security.core.userdetails.User(client.getUsername(), client.getPassword(),
                client.isEnabled(), true, true, true,
                Arrays.asList(new SimpleGrantedAuthority(client.getStatus())));
    }

    /*
        Метод isExist() возвращает, существует ли пользователь с таким юзернеймом
     */
    public boolean isExist(String username) {
        Optional<Client> data = clientRepo.findByUsername(username);
        return data.isPresent();
    }

    public Client save(UserCred client) {
        Client newUser = new Client();
        newUser.setUsername(client.getUsername());
        newUser.setPassword(bcryptEncoder.encode(client.getPassword()));
        newUser.setEnabled(true);
        newUser.setStatus("ROLE_USER");
        return clientRepo.save(newUser);
        //?
    }
}
