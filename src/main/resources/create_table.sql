CREATE TABLE category
(
    id serial PRIMARY KEY,
    name text NOT NULL
);

CREATE TABLE client
(
    id serial PRIMARY KEY,
    first_name character varying(255),
    last_name character varying(255),
    address text,
    bonus numeric DEFAULT 0,
    status text NOT NULL,
    username text NOT NULL,
    password text NOT NULL,
    enabled boolean DEFAULT true,
    imageURL text
);

CREATE TABLE order_table
(
    id serial PRIMARY KEY,
    client_id integer NOT NULL,
    status bool NOT NULL,
    shipping_date date,
    amount numeric not null,
    CONSTRAINT order_client_id_fkey FOREIGN KEY (client_id)
        REFERENCES client (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);


CREATE TABLE product
(
    id serial NOT NULL,
    name text NOT NULL,
    price numeric NOT NULL,
    category_id integer NOT NULL,
    client_id integer NOT NULL,
    text text,
    imageURL text,
    quantity integer NOT NULL,
    code text not null,
    existence boolean DEFAULT true,
    valide boolean default false,
    CONSTRAINT product_pkey PRIMARY KEY (id),
    CONSTRAINT product_category_id_fkey FOREIGN KEY (category_id)
        REFERENCES category (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT product_client_id_fkey FOREIGN KEY (client_id)
        REFERENCES client (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

CREATE TABLE order_item
(
    id serial NOT NULL,
    order_id integer NOT NULL,
    product_id integer NOT NULL,
    price numeric NOT NULL,
    quantity integer NOT NULL,
    CONSTRAINT orderproduct_pkey PRIMARY KEY (id),
    CONSTRAINT orderproduct_order_id_fkey FOREIGN KEY (order_id)
        REFERENCES order_table (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT orderproduct_product_id_fkey FOREIGN KEY (product_id)
        REFERENCES public.product (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
);

CREATE TABLE money_claim
(
    id serial PRIMARY KEY,
    client_id integer NOT NULL REFERENCES client(id) ON DELETE CASCADE,
    amount numeric NOT NULL,
    creation_date date NOT NULL,
    review_date date,
    status boolean
);

CREATE OR REPLACE FUNCTION add_product() RETURNS trigger AS $add_product$
declare
    pos record;
    BEGIN
		for pos in select name, price, client_id, category_id, text, imageURL, code  from product
		loop
			if new.name=pos.name and new.price = pos.price and new.client_id = pos.client_id and new.code = pos.code
			       and new.category_id = pos.category_id  and (new.text = pos.text or (new.text is null and pos.text is null) or (new.text = '' and  pos.text is null) or (new.text is null and pos.text = ''))
			       and (new.imageURL = pos.imageURL or (new.imageURL is null and pos.imageURL  is null) or (new.imageURL = '' and pos.imageURL is null) or(new.imageURL is null and pos.imageURL = ''))
			then
			    if pos.text is null and pos.imageURL is not null then
				    new.quantity = new.quantity + (select quantity from product where name = pos.name and price = pos.price
				    and client_id = pos.client_id and category_id = pos.category_id and text is null and imageURL = pos.imageURL and code = pos.code);
				    new.id = (select id from product where name = pos.name and price = pos.price
                    and client_id = pos.client_id and category_id = pos.category_id and
                    text is null and imageURL = pos.imageURL and valide = pos.valide and code = pos.code and code = pos.code);
                   	delete from product where name = pos.name and price = pos.price
                    and client_id = pos.client_id and category_id = pos.category_id and text is null and imageURL = pos.imageURL  and code = pos.code;
                end if;
                if pos.imageURL is null and pos.text is not null then
           				    new.quantity = new.quantity + (select quantity from product where name = pos.name and price = pos.price
             			    and client_id = pos.client_id and category_id = pos.category_id and text = pos.text and imageURL is null and code = pos.code);
             			    new.id = (select id from product where name = pos.name and price = pos.price
                            and client_id = pos.client_id and category_id = pos.category_id and
                            text = pos.text and imageURL is null and valide = pos.valide and code = pos.code);
                            delete from product where name = pos.name and price = pos.price
                            and client_id = pos.client_id and category_id = pos.category_id and text = pos.text and imageURL is null and code = pos.code;
                end if;

                if pos.imageURL is null and pos.text is null then
                      		    new.quantity = new.quantity + (select quantity from product where name = pos.name and price = pos.price
                 			    and client_id = pos.client_id and category_id = pos.category_id and text is null and imageURL is null);
                 			    new.id = (select id from product where name = pos.name and price = pos.price
                                and client_id = pos.client_id and category_id = pos.category_id and
                                text is null and imageURL is null and code = pos.code);
                                delete from product where name = pos.name and price = pos.price
                                and client_id = pos.client_id and category_id = pos.category_id and text is null and imageURL is null and code = pos.code;
                end if;

                if pos.imageURL is not null and pos.text is not null then
                         new.quantity = new.quantity + (select quantity from product where name = pos.name and price = pos.price
          			     and client_id = pos.client_id and category_id = pos.category_id and text = pos.text and imageURL = pos.imageURL  and code = pos.code);
				         new.id = (select id from product where name = pos.name and price = pos.price
                         and client_id = pos.client_id and category_id = pos.category_id and
                         text = pos.text and imageURL = pos.imageURL  and code = pos.code);
			             delete from product where name = pos.name and price = pos.price
                         and client_id = pos.client_id and category_id = pos.category_id and text = pos.text and imageURL = pos.imageURL  and code = pos.code;
                 end if;
			end if;
		end loop;
		return new;
    END;
$add_product$ LANGUAGE plpgsql;

CREATE TRIGGER add_product before INSERT  ON product
    FOR EACH ROW EXECUTE FUNCTION add_product();



CREATE OR REPLACE FUNCTION add_product_to_basket() RETURNS trigger AS $add_product_to_basket$
declare
    pos record;
    BEGIN
		for pos in select name, price, order_id, product_id, quantity from order_item
		loop
			if new.name=pos.name and new.price = pos.price/pos.quantity and new.order_id = pos.order_id
			and new.product_id = pos.product_id
			then
				new.quantity = new.quantity + pos.quantity;
				new.id = (select id from order_item where name = pos.name and price = pos.price
										  and product_id = pos.product_id and order_id = pos.order_id);
				new.price = new.price + pos.price;
				delete from order_item where  name = pos.name and price = pos.price
										  and product_id = pos.product_id and order_id = pos.order_id;
			end if;
		end loop;
		return new;
    END;
$add_product_to_basket$ LANGUAGE plpgsql;

CREATE TRIGGER add_product_to_basket before INSERT  ON order_item
    FOR EACH ROW EXECUTE FUNCTION add_product_to_basket();

